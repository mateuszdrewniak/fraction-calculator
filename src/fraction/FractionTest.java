package fraction;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FractionTest {

	@Test
	void testFractionConstruction() {
		Fraction fraction1;

		fraction1 = new Fraction(1, -16, 8);
		assert fraction1.isNegative();
		assertEquals("(-3)", fraction1.toString());
		fraction1.toImproper();
		assertEquals("(-3/1)", fraction1.toString());
		fraction1.simplify();
		assertEquals("(-3)", fraction1.toString());
		fraction1.toReciprocal();
		assertEquals("(-1/3)", fraction1.toString());
		
		fraction1 = new Fraction(-1, -16, -8);
		assert fraction1.isNegative();
		assertEquals("(-3)", fraction1.toString());
		
		fraction1 = new Fraction(-1, -16, 8);
		assert !fraction1.isNegative();
		assertEquals("(3)", fraction1.toString());
		
		fraction1 = new Fraction(1, -16, -8);
		assert !fraction1.isNegative();
		assertEquals("(3)", fraction1.toString());
		
		fraction1 = new Fraction(-1, 3, 9);
		assert fraction1.isNegative();
		assertEquals("(-1 1/3)", fraction1.toString());
		fraction1.toImproper();
		assertEquals("(-4/3)", fraction1.toString());
		fraction1.simplify();
		assertEquals("(-1 1/3)", fraction1.toString());
		fraction1.toReciprocal();
		assertEquals("(-3/4)", fraction1.toString());
		
		fraction1 = new Fraction(0, 20, 8);
		assert !fraction1.isNegative();
		assertEquals("(2 1/2)", fraction1.toString());
		fraction1.toImproper();
		assertEquals("(5/2)", fraction1.toString());
		fraction1.simplify();
		assertEquals("(2 1/2)", fraction1.toString());
		fraction1.toReciprocal();
		assertEquals("(2/5)", fraction1.toString());
		
		fraction1 = new Fraction(0, 1, 1, true);
		assert fraction1.isNegative();
		assertEquals("(-1)", fraction1.toString());
		
		fraction1 = new Fraction(0, 1, 1, false);
		assert !fraction1.isNegative();
		assertEquals("(1)", fraction1.toString());
		
		fraction1 = new Fraction(0, 0, 0, false);
		assert !fraction1.isNegative();
		assertEquals("(0)", fraction1.toString());
		
		fraction1 = new Fraction(0, 0, 0, true);
		assert !fraction1.isNegative();
		assertEquals("(0)", fraction1.toString());
		
		fraction1 = new Fraction(0, 4, 1, true);
		assert fraction1.isNegative();
		assertEquals("(-4)", fraction1.toString());
		
		fraction1 = new Fraction(4, 1, 1, true);
		assert fraction1.isNegative();
		assertEquals("(-4)", fraction1.toString());
		
		fraction1 = new Fraction(0, 3, 4, true);
		assert fraction1.isNegative();
		assertEquals("(-3/4)", fraction1.toString());
		
		fraction1 = new Fraction(0, 3, 4, false);
		assert !fraction1.isNegative();
		assertEquals("(3/4)", fraction1.toString());
		
		fraction1 = new Fraction(1, 5, 4, false, false);
		assert !fraction1.isNegative();
		assertEquals("(1 5/4)", fraction1.toString());
		
		fraction1 = new Fraction(1, 5, 4, true, false);
		assert fraction1.isNegative();
		assertEquals("(-1 5/4)", fraction1.toString());
		
		fraction1 = new Fraction(1, 5, 4, true, true);
		assert fraction1.isNegative();
		assertEquals("(-2 1/4)", fraction1.toString());
	}
	
	@Test
	void testFractionConstructionFromString() {
		Fraction fraction1;
		Fraction fraction2;
		
		fraction1 = new Fraction("(-20)");
		assert fraction1.isNegative();
		assertEquals("(-20)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		
		fraction1 = new Fraction("(1)");
		assert !fraction1.isNegative();
		assertEquals("(1)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		
		fraction1 = new Fraction("(-3)");
		assert fraction1.isNegative();
		assertEquals("(-3)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		
		fraction1 = new Fraction("(-0)");
		assert !fraction1.isNegative();
		assertEquals("(0)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		
		fraction1 = new Fraction("(-2 1/4)");
		assert fraction1.isNegative();
		assertEquals("(-2 1/4)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		
		fraction1 = new Fraction("(-9/4)");
		assert fraction1.isNegative();
		assertEquals("(-2 1/4)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		
		fraction1 = new Fraction("(9/4)");
		assert !fraction1.isNegative();
		assertEquals("(2 1/4)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		
		fraction1 = new Fraction("(1 -99/-4)");
		assert !fraction1.isNegative();
		assertEquals("(25 3/4)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		
		// Double String
		
		fraction1 = new Fraction("(2.2)");
		assert !fraction1.isNegative();
		assertEquals("(2 1/5)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		assertEquals(2.2, fraction1.toDouble());
		
		fraction1 = new Fraction("(2.5)");
		assert !fraction1.isNegative();
		assertEquals("(2 1/2)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		assertEquals(2.5, fraction1.toDouble());
		
		fraction1 = new Fraction("(-2.2)");
		assert fraction1.isNegative();
		assertEquals("(-2 1/5)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		assertEquals(-2.2, fraction1.toDouble());
		
		fraction1 = new Fraction("(0.0)");
		assert !fraction1.isNegative();
		assertEquals("(0)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		assertEquals(0.0, fraction1.toDouble());
		
		fraction1 = new Fraction("(2.0)");
		assert !fraction1.isNegative();
		assertEquals("(2)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		assertEquals(2.0, fraction1.toDouble());
		
		fraction1 = new Fraction("(1000.0)");
		assert !fraction1.isNegative();
		assertEquals("(1000)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		assertEquals(1000.0, fraction1.toDouble());
		
		fraction1 = new Fraction("(2.1456)");
		assert !fraction1.isNegative();
		assertEquals("(2 91/625)", fraction1.toString());
		fraction2 = new Fraction(fraction1.toString());
		assertEquals(fraction1.toString(), fraction2.toString());
		assertEquals(2.1456, fraction1.toDouble());
	}
	
	@Test
	void testFractionConstructionFromDouble() {
		Fraction fraction;

		fraction = new Fraction(2.2);
		assert !fraction.isNegative();
		assertEquals("(2 1/5)", fraction.toString());
		assertEquals(2.2, fraction.toDouble());
		
		fraction = new Fraction(2.5);
		assert !fraction.isNegative();
		assertEquals("(2 1/2)", fraction.toString());
		assertEquals(2.5, fraction.toDouble());
		
		fraction = new Fraction(-2.2);
		assert fraction.isNegative();
		assertEquals("(-2 1/5)", fraction.toString());
		assertEquals(-2.2, fraction.toDouble());
		
		fraction = new Fraction(-2.5);
		assert fraction.isNegative();
		assertEquals("(-2 1/2)", fraction.toString());
		assertEquals(-2.5, fraction.toDouble());
		
		fraction = new Fraction(0.0);
		assert !fraction.isNegative();
		assertEquals("(0)", fraction.toString());
		assertEquals(0.0, fraction.toDouble());
		
		fraction = new Fraction(2);
		assert !fraction.isNegative();
		assertEquals("(2)", fraction.toString());
		assertEquals(2.0, fraction.toDouble());
		
		fraction = new Fraction(1000f);
		assert !fraction.isNegative();
		assertEquals("(1000)", fraction.toString());
		assertEquals(1000.0, fraction.toDouble());
		
		fraction = new Fraction(2.1456);
		assert !fraction.isNegative();
		assertEquals("(2 91/625)", fraction.toString());
		assertEquals(2.1456, fraction.toDouble());
	}
	
	@Test
	void testFractionValidSyntax() {
		assert !Fraction.validSyntax("(-2 -1-/-2)");
		assert !Fraction.validSyntax("(-2 1/2");
		assert Fraction.validSyntax("(-2 -1/-2)");
		assert Fraction.validSyntax("(-2 1/2)");
		
		assert Fraction.validSyntax("(1/2)");
		assert Fraction.validSyntax("(-135/20)");
		assert Fraction.validSyntax("(20)");
		assert Fraction.validSyntax("(0)");
		
		assert !Fraction.validSyntax("(2.0.0)");
		assert !Fraction.validSyntax("(2.0");
		assert !Fraction.validSyntax("2.0)");
		assert !Fraction.validSyntax("2.0");
		assert !Fraction.validSyntax("(-2.-0)");
		assert !Fraction.validSyntax("(.523)");
		
		assert Fraction.validSyntax("(-2.5)");
		assert Fraction.validSyntax("(2.0)");
		assert Fraction.validSyntax("(2.5)");
		assert Fraction.validSyntax("(1.2)");
		assert Fraction.validSyntax("(0.125)");
		assert Fraction.validSyntax("(10231343.1243324)");
	}
	
	@Test
	void testMultiplicationDivision() {
		Fraction fraction1;
		Fraction fraction2;
		
		fraction1 = new Fraction(0, 3, 4);
		fraction2 = new Fraction(1, 1, 3);
		fraction1.multiply(fraction2);
		assertEquals("(1 1/3)", fraction2.toString());
		assertEquals("(1)", fraction1.toString());
		
		fraction1 = new Fraction(0, 3, 4);
		fraction2 = new Fraction(-1, 1, 6);
		fraction1.multiply(fraction2);
		assertEquals("(-7/8)", fraction1.toString());
		fraction1.multiply(fraction2);
		assertEquals("(1 1/48)", fraction1.toString());
		fraction1.divide(fraction2);
		assertEquals("(-7/8)", fraction1.toString());
		
		fraction1 = new Fraction(-1, 1, 6);
		fraction2 = new Fraction(-1, 1, 6);
		fraction1.divide(fraction2);
		assertEquals("(1)", fraction1.toString());
		
		fraction1 = new Fraction(-1, 1, 6);
		fraction2 = new Fraction(1, 1, 6);
		fraction1.divide(fraction2);
		assertEquals("(-1)", fraction1.toString());
		
		fraction1 = new Fraction(0, 8, 9);
		fraction2 = new Fraction(1, 1, 3);
		fraction1.divide(fraction2);
		assertEquals("(2/3)", fraction1.toString());
	}
	
	@Test
	void testAdditionSubstraction() {
		Fraction fraction1;
		Fraction fraction2;
		
		fraction1 = new Fraction(0, 6, 4);
		fraction2 = new Fraction(0, 1, 2);
		fraction1.add(fraction2);
		assertEquals("(2)", fraction1.toString());
		
		fraction1 = new Fraction(0, 5, 8);
		fraction2 = new Fraction(0, 13, 16);
		fraction1.add(fraction2);
		assertEquals("(1 7/16)", fraction1.toString());
		
		fraction1 = new Fraction(-2, 1, 4);
		fraction2 = new Fraction(-1, 3, 5);
		fraction1.substract(fraction2);
		assertEquals("(-13/20)", fraction1.toString());
		
		fraction1 = new Fraction(-2, 1, 4);
		fraction2 = new Fraction(1, 3, 5);
		fraction1.substract(fraction2);
		assertEquals("(-3 17/20)", fraction1.toString());
		
		fraction1 = new Fraction(-2, 1, 4);
		fraction2 = new Fraction(-1, 3, 5);
		fraction1.add(fraction2);
		assertEquals("(-3 17/20)", fraction1.toString());
		
		fraction1 = new Fraction(0, 0, 0);
		fraction2 = new Fraction(-1, 3, 5);
		fraction1.add(fraction2);
		assertEquals("(-1 3/5)", fraction1.toString());
		
		fraction1 = new Fraction(0, 0, 0);
		fraction2 = new Fraction(-1, 3, 5);
		fraction1.substract(fraction2);
		assertEquals("(1 3/5)", fraction1.toString());
		
		fraction1 = new Fraction(-1, 3, 5);
		fraction2 = new Fraction(0, 0, 0);
		fraction1.substract(fraction2);
		assertEquals("(-1 3/5)", fraction1.toString());
		
		fraction1 = new Fraction(-1, 3, 5);
		fraction2 = new Fraction(0, 0, 0);
		fraction1.add(fraction2);
		assertEquals("(-1 3/5)", fraction1.toString());
		
		fraction1 = new Fraction(-1, 3, 5);
		fraction2 = new Fraction(1, 3, 5);
		fraction1.add(fraction2);
		assertEquals("(0)", fraction1.toString());
		
		fraction1 = new Fraction(1, 3, 5);
		fraction2 = new Fraction(1, 3, 5);
		fraction1.substract(fraction2);
		assertEquals("(0)", fraction1.toString());
		
		fraction1 = new Fraction(0, 0, 0);
		fraction2 = new Fraction(1, 3, 5);
		fraction1.substract(fraction2);
		assertEquals("(-1 3/5)", fraction1.toString());
		
		fraction1 = new Fraction(0, 4, 1);
		fraction2 = new Fraction(0, 4, 1);
		fraction1.substract(fraction2);
		assertEquals("(0)", fraction1.toString());
	}

	@Test
	void testPower() {
		Fraction fraction1;
		
		fraction1 = new Fraction(1, 1, 3);
		fraction1.power(2);
		assertEquals("(1 7/9)", fraction1.toString());
		
		fraction1 = new Fraction(1, 1, 3);
		fraction1.power(-1);
		assertEquals("(3/4)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 3);
		fraction1.power(3);
		assertEquals("(-2 10/27)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 3);
		fraction1.power(-3);
		assertEquals("(-27/64)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 3);
		fraction1.power(-2);
		assertEquals("(9/16)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 1);
		fraction1.power(-2);
		assertEquals("(1/16)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 1);
		fraction1.power(2);
		assertEquals("(16)", fraction1.toString());
		
		
		fraction1 = new Fraction(1, 1, 3);
		fraction1.power(new Fraction(2, 1, 1));
		assertEquals("(1 7/9)", fraction1.toString());
		
		fraction1 = new Fraction(1, 1, 3);
		fraction1.power(new Fraction(-1, 1, 1));
		assertEquals("(3/4)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 3);
		fraction1.power(new Fraction(3, 1, 1));
		assertEquals("(-2 10/27)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 3);
		fraction1.power(new Fraction(-3, 1, 1));
		assertEquals("(-27/64)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 3);
		fraction1.power(new Fraction(-2, 1, 1));
		assertEquals("(9/16)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 1);
		fraction1.power(new Fraction(-2, 1, 1));
		assertEquals("(1/16)", fraction1.toString());
		
		fraction1 = new Fraction(0, -4, 1);
		fraction1.power(new Fraction(2, 1, 1));
		assertEquals("(16)", fraction1.toString());
	}
	
	@Test
	void testToDouble() {
		Fraction fraction1;
		
		fraction1 = new Fraction(0, -4, 1);
		assertEquals(-4.0, fraction1.toDouble());
		
		fraction1 = new Fraction(0, 15, 20);
		assertEquals(0.75, fraction1.toDouble());
		
		fraction1 = new Fraction(1, 11, 20);
		assertEquals(1.55, fraction1.toDouble());
		
		fraction1 = new Fraction(21, -81, 99);
		assertEquals(-21.818181818181817, fraction1.toDouble());
	}
}
