package fraction;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.util.Objects;
import java.beans.PropertyChangeEvent;
import javax.swing.JToggleButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;

public class FractionCalculatorGUI {

	private JFrame frame;
	private JTextField textField;
	private JLabel lastFormula;
	private JLabel labelIncorrectSyntax;
	private JLabel labelTheme;
	private JLabel labelVersion;

	private JButton buttonBackspace;
	private JButton buttonCE;
	private JButton buttonOne;
	private JButton buttonTwo;
	private JButton buttonThree;
	private JButton buttonFour;
	private JButton buttonFive;
	private JButton buttonSix;
	private JButton buttonSeven;
	private JButton buttonEight;
	private JButton buttonNine;
	private JButton buttonZero;
	private JButton buttonAdd;
	private JButton buttonSubstract;
	private JButton buttonDivide;
	private JButton buttonMultiply;
	private JButton buttonPower;
	private JButton buttonResult;
	private JButton buttonDecimalSeparator;
	private JButton buttonCloseBracket;
	private JButton buttonOpenBracket;
	private JButton buttonSpace;

	private ButtonGroup languageRadio;
	private JRadioButton radioEnglish;
	private JRadioButton radioPolish;
	
	private JToggleButton decimalFractionResultToggle;
	
	private JCheckBox checkboxDarkTheme;
	private Color defaultBackground;

	private String polishError = "Nieprawidłowa składnia!";
	private String polishDecimalToggle = "Dziesiętny";
	private String polishDarkThemeCheckbox = "Ciemny";
	
	private String englishError = "Incorrect syntax!";
	private String englishDecimalToggle = "Decimal";
	private String englishDarkThemeCheckbox = "Dark";
	
	private String localizedError = englishError;
	private String localizedDecimalToggle = englishDecimalToggle;
	private String localizedDarkThemeCheckbox = englishDarkThemeCheckbox;
	private String theme = "default";
	private String version = "v1.0.1";

	private boolean errorPresent = false;
	private boolean resultPresent = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FractionCalculatorGUI window = new FractionCalculatorGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FractionCalculatorGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Fraction Calculator");
		frame.setResizable(false);
		frame.setBounds(100, 100, 385, 610);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		defaultBackground = frame.getContentPane().getBackground();
		
		Font buttonFont = new Font("Microsoft JhengHei UI", Font.BOLD, 16);
		Font operatorFont = new Font("Magnum Gothic", Font.BOLD, 18);
		
		lastFormula = new JLabel("");
		lastFormula.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 11));
		lastFormula.setBounds(40, 11, 295, 25);
		frame.getContentPane().add(lastFormula);
		
		textField = new JTextField();
		textField.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 13));
		textField.setBounds(40, 33, 295, 60);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		addChangeListener(textField, e -> formulaChanged());
		
		buttonBackspace = new JButton("<--");
		buttonBackspace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteLastCharFromTextField();
			}
		});
		buttonBackspace.setFont(operatorFont);
		buttonBackspace.setBounds(190, 130, 145, 60);
		frame.getContentPane().add(buttonBackspace);
		
		buttonCE = new JButton("CE");
		buttonCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resetCalculator();
			}
		});
		buttonCE.setFont(operatorFont);
		buttonCE.setBounds(115, 130, 70, 60);
		frame.getContentPane().add(buttonCE);
		
		buttonDivide = new JButton("/");
		buttonDivide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!lastCharIsAnOperator())
					addCharacterToTextField('/');
			}
		});
		buttonDivide.setFont(operatorFont);
		buttonDivide.setBounds(265, 198, 70, 60);
		frame.getContentPane().add(buttonDivide);
		
		buttonPower = new JButton("^");
		buttonPower.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!lastCharIsAnOperator())
					addCharacterToTextField('^');
			}
		});
		buttonPower.setFont(operatorFont);
		buttonPower.setBounds(190, 198, 70, 60);
		frame.getContentPane().add(buttonPower);
		
		buttonCloseBracket = new JButton(")");
		buttonCloseBracket.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField(')');
			}
		});
		buttonCloseBracket.setFont(operatorFont);
		buttonCloseBracket.setBounds(115, 198, 70, 60);
		frame.getContentPane().add(buttonCloseBracket);
		
		buttonOpenBracket = new JButton("(");
		buttonOpenBracket.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('(');
			}
		});
		buttonOpenBracket.setFont(operatorFont);
		buttonOpenBracket.setBounds(40, 198, 70, 60);
		frame.getContentPane().add(buttonOpenBracket);
		
		buttonMultiply = new JButton("*");
		buttonMultiply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!lastCharIsAnOperator())
					addCharacterToTextField('*');
			}
		});
		buttonMultiply.setFont(operatorFont);
		buttonMultiply.setBounds(265, 269, 70, 60);
		frame.getContentPane().add(buttonMultiply);
		
		buttonNine = new JButton("9");
		buttonNine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('9');
			}
		});
		buttonNine.setFont(buttonFont);
		buttonNine.setBounds(190, 269, 70, 60);
		frame.getContentPane().add(buttonNine);
		
		buttonEight = new JButton("8");
		buttonEight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('8');
			}
		});
		buttonEight.setFont(buttonFont);
		buttonEight.setBounds(115, 269, 70, 60);
		frame.getContentPane().add(buttonEight);
		
		buttonSeven = new JButton("7");
		buttonSeven.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('7');
			}
		});
		buttonSeven.setFont(buttonFont);
		buttonSeven.setBounds(40, 269, 70, 60);
		frame.getContentPane().add(buttonSeven);
		
		buttonFour = new JButton("4");
		buttonFour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('4');
			}
		});
		buttonFour.setFont(buttonFont);
		buttonFour.setBounds(40, 339, 70, 60);
		frame.getContentPane().add(buttonFour);
		
		buttonFive = new JButton("5");
		buttonFive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('5');
			}
		});
		buttonFive.setFont(buttonFont);
		buttonFive.setBounds(115, 339, 70, 60);
		frame.getContentPane().add(buttonFive);
		
		buttonSix = new JButton("6");
		buttonSix.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('6');
			}
		});
		buttonSix.setFont(buttonFont);
		buttonSix.setBounds(190, 339, 70, 60);
		frame.getContentPane().add(buttonSix);
		
		buttonSubstract = new JButton("-");
		buttonSubstract.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!lastCharIsAnOperator())
					addCharacterToTextField('-');
			}
		});
		buttonSubstract.setFont(operatorFont);
		buttonSubstract.setBounds(265, 339, 70, 60);
		frame.getContentPane().add(buttonSubstract);
		
		buttonAdd = new JButton("+");
		buttonAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!lastCharIsAnOperator())
					addCharacterToTextField('+');
			}
		});
		buttonAdd.setFont(operatorFont);
		buttonAdd.setBounds(265, 410, 70, 60);
		frame.getContentPane().add(buttonAdd);
		
		buttonThree = new JButton("3");
		buttonThree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('3');
			}
		});
		buttonThree.setFont(buttonFont);
		buttonThree.setBounds(190, 410, 70, 60);
		frame.getContentPane().add(buttonThree);
		
		buttonTwo = new JButton("2");
		buttonTwo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('2');
			}
		});
		buttonTwo.setFont(buttonFont);
		buttonTwo.setBounds(115, 410, 70, 60);
		frame.getContentPane().add(buttonTwo);
		
		buttonOne = new JButton("1");
		buttonOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('1');
			}
		});
		buttonOne.setFont(buttonFont);
		buttonOne.setBounds(40, 410, 70, 60);
		frame.getContentPane().add(buttonOne);
		
		buttonResult = new JButton("=");
		buttonResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getResult();
			}
		});
		buttonResult.setFont(operatorFont);
		buttonResult.setBounds(265, 481, 70, 60);
		frame.getContentPane().add(buttonResult);
		
		buttonZero = new JButton("0");
		buttonZero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCharacterToTextField('0');
			}
		});
		buttonZero.setFont(buttonFont);
		buttonZero.setBounds(115, 481, 70, 60);
		frame.getContentPane().add(buttonZero);
		
		buttonDecimalSeparator = new JButton(".");
		buttonDecimalSeparator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!lastCharIsAnOperator())
					addCharacterToTextField('.');
			}
		});
		buttonDecimalSeparator.setFont(buttonFont);
		buttonDecimalSeparator.setBounds(40, 481, 70, 60);
		frame.getContentPane().add(buttonDecimalSeparator);
		
		labelIncorrectSyntax = new JLabel();
		labelIncorrectSyntax.setForeground(Color.RED);
		labelIncorrectSyntax.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 12));
		labelIncorrectSyntax.setBounds(190, 94, 139, 25);
		frame.getContentPane().add(labelIncorrectSyntax);
		
		labelTheme = new JLabel("default");
		labelTheme.setBounds(320, 550, 50, 14);
		frame.getContentPane().add(labelTheme);
		
		labelVersion = new JLabel(version);
		labelVersion.setBounds(10, 550, 50, 14);
		frame.getContentPane().add(labelVersion);
		
		radioEnglish = new JRadioButton("English");
		radioEnglish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setLanguage(0);
			}
		});
		radioEnglish.setSelected(true);
		radioEnglish.setBounds(40, 130, 70, 23);
		frame.getContentPane().add(radioEnglish);
		
		radioPolish = new JRadioButton("Polski");
		radioPolish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setLanguage(1);
			}
		});
		radioPolish.setBounds(40, 148, 70, 23);
		frame.getContentPane().add(radioPolish);
		
		languageRadio = new ButtonGroup();
		languageRadio.add(radioEnglish);
		languageRadio.add(radioPolish);
		
		decimalFractionResultToggle = new JToggleButton(localizedDecimalToggle);
		decimalFractionResultToggle.setBounds(40, 94, 97, 23);
		frame.getContentPane().add(decimalFractionResultToggle);
		
		buttonSpace = new JButton("_");
		buttonSpace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addCharacterToTextField(' ');
			}
		});
		buttonSpace.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		buttonSpace.setBounds(190, 481, 70, 60);
		frame.getContentPane().add(buttonSpace);
		
		checkboxDarkTheme = new JCheckBox("Dark");
		checkboxDarkTheme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(checkboxDarkTheme.isSelected())
					setThemeDark();
				else
					setThemeDefault();
			}
		});
		checkboxDarkTheme.setBounds(40, 167, 69, 23);
		frame.getContentPane().add(checkboxDarkTheme);
	}
	
	
	// tribute to https://stackoverflow.com/users/964243/boann from https://stackoverflow.com/questions/3953208/value-change-listener-to-jtextfield
	public static void addChangeListener(JTextComponent text, ChangeListener changeListener) {
	    Objects.requireNonNull(text);
	    Objects.requireNonNull(changeListener);
	    DocumentListener dl = new DocumentListener() {
	        private int lastChange = 0, lastNotifiedChange = 0;

	        @Override
	        public void insertUpdate(DocumentEvent e) {
	            changedUpdate(e);
	        }

	        @Override
	        public void removeUpdate(DocumentEvent e) {
	            changedUpdate(e);
	        }

	        @Override
	        public void changedUpdate(DocumentEvent e) {
	            lastChange++;
	            SwingUtilities.invokeLater(() -> {
	                if (lastNotifiedChange != lastChange) {
	                    lastNotifiedChange = lastChange;
	                    changeListener.stateChanged(new ChangeEvent(text));
	                }
	            });
	        }
	    };
	    text.addPropertyChangeListener("document", (PropertyChangeEvent e) -> {
	        Document d1 = (Document)e.getOldValue();
	        Document d2 = (Document)e.getNewValue();
	        if (d1 != null) d1.removeDocumentListener(dl);
	        if (d2 != null) d2.addDocumentListener(dl);
	        dl.changedUpdate(null);
	    });
	    Document d = text.getDocument();
	    if (d != null) d.addDocumentListener(dl);
	}
	
	private void getResult() {
		String currentFormula = textField.getText();
		secretCodes();
		if(!errorPresent && !resultPresent) {
			Fraction result = FractionCalculator.evaluateExpression(currentFormula);
			if(result != null) {
				String resultString = "";
				labelIncorrectSyntax.setText("");
				errorPresent = false;
				lastFormula.setText(currentFormula + '=');
				if(decimalFractionResultToggle.isSelected()) resultString = "(" + Double.toString(result.toDouble()) + ")";
				else resultString = result.toString();
				textField.setText(resultString);
				resultPresent = true;
			}
			else {
				labelIncorrectSyntax.setText(localizedError);
				errorPresent = true;
			}
		}
	}
	
	private boolean lastCharIsAnOperator() {
		String currentFormula = textField.getText();
		currentFormula.trim();
		if(currentFormula != null && currentFormula.length() > 0) {
			char last = currentFormula.charAt(currentFormula.length() - 1);
			switch(last) {
				case '+':
				case '-':
				case '*':
				case '/':
				case '^':
				case '.':
					return true;
			}
		}
		
		return false;
	}
	
	private void secretCodes() {
		String currentFormula = textField.getText();
		currentFormula.trim();
		
		
		if(currentFormula.equals("(olivia)"))
			setThemeOlivia();
		else if(currentFormula.equals("(jula)")) {
			if(!theme.equals("jula"))
				setThemeJula();
		}
		else if(currentFormula.equals("(default)")) {
			if(!theme.equals("default"))
				setThemeDefault();
		}
		else if(currentFormula.equals("(dark)")) {
			if(!theme.equals("dark"))
				setThemeDark();
		}
	}
	
	private void setThemeDefault() {
		Color color;
		JTextField newTextField = new JTextField();

		frame.getContentPane().setBackground(defaultBackground);
		radioEnglish.setBackground(defaultBackground);
		radioPolish.setBackground(defaultBackground);
		checkboxDarkTheme.setBackground(defaultBackground);
		textField.setBackground(newTextField.getBackground());
		textField.setBorder(newTextField.getBorder());
		textField.setForeground(newTextField.getForeground());
		textField.setCaretColor(newTextField.getCaretColor());
		radioEnglish.setForeground(newTextField.getForeground());
		radioPolish.setForeground(newTextField.getForeground());
		checkboxDarkTheme.setForeground(newTextField.getForeground());
		lastFormula.setForeground(newTextField.getForeground());
		labelIncorrectSyntax.setForeground(Color.RED);
		
		setDefaultButtonColor();
		
		theme = "default";
		labelTheme.setText(theme);
	}
	
	private void setThemeDark() {
		Color color, main, background, borderColor, secondary;
		
		background = new Color(18, 18, 18);
		frame.getContentPane().setBackground(background);
		radioEnglish.setBackground(background);
		radioPolish.setBackground(background);
		checkboxDarkTheme.setBackground(background);
		
		borderColor = new Color(55, 54, 57);
		textField.setBackground(borderColor);
		Border border;
		border = BorderFactory.createLineBorder(borderColor);
		textField.setBorder(border);
		
		secondary = new Color(225, 225, 225);
		textField.setForeground(secondary);
		textField.setCaretColor(secondary);
		radioEnglish.setForeground(secondary);
		radioPolish.setForeground(secondary);
		checkboxDarkTheme.setForeground(secondary);
		lastFormula.setForeground(secondary);
		
		main = new Color(195, 143, 255);
		labelIncorrectSyntax.setForeground(main);
		
		secondary = new Color(158, 158, 158);
		setButtonColor(main, background, secondary, borderColor);
		
		theme = "dark";
		labelTheme.setText(theme);
	}
	
	private void setButtonColor(Color main, Color background, Color secondary, Color borderColor) {
		setElementColors(buttonResult, main, background, borderColor);
		setElementColors(buttonCE, main, background, borderColor);
		setElementColors(decimalFractionResultToggle, main, background, borderColor);
		
		setElementColors(buttonBackspace, background, main, borderColor);
		setElementColors(buttonOpenBracket, background, main, borderColor);
		setElementColors(buttonCloseBracket, background, main, borderColor);
		setElementColors(buttonPower, background, main, borderColor);
		setElementColors(buttonDivide, background, main, borderColor);
		setElementColors(buttonMultiply, background, main, borderColor);
		setElementColors(buttonSubstract, background, main, borderColor);
		setElementColors(buttonAdd, background, main, borderColor);
		
		setElementColors(buttonOne, background, secondary, borderColor);
		setElementColors(buttonTwo, background, secondary, borderColor);
		setElementColors(buttonThree, background, secondary, borderColor);
		setElementColors(buttonFour, background, secondary, borderColor);
		setElementColors(buttonFive, background, secondary, borderColor);
		setElementColors(buttonSix, background, secondary, borderColor);
		setElementColors(buttonSeven, background, secondary, borderColor);
		setElementColors(buttonEight, background, secondary, borderColor);
		setElementColors(buttonNine, background, secondary, borderColor);
		setElementColors(buttonZero, background, secondary, borderColor);
		setElementColors(buttonSpace, background, secondary, borderColor);
		setElementColors(buttonDecimalSeparator, background, secondary, borderColor);
	}
	
	private void setDefaultButtonColor() {
		JButton newButton = new JButton();
		
		setElementColors(buttonResult, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonCE, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(decimalFractionResultToggle, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonBackspace, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonOpenBracket, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonCloseBracket, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonPower, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonDivide, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonMultiply, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonSubstract, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonAdd, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonOne, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonTwo, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonThree, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonFour, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonFive, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonSix, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonSeven, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonEight, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonNine, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonZero, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonSpace, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
		setElementColors(buttonDecimalSeparator, newButton.getBackground(), newButton.getForeground(), newButton.getBorder());
	}
	
	private void setElementColors(JComponent element, Color background, Color foreground, Color borderColor) {
		Border border;
		border = BorderFactory.createLineBorder(borderColor);
		element.setBackground(background);
		element.setForeground(foreground);
		element.setBorder(border);
	}
	
	private void setElementColors(JComponent element, Color background, Color foreground, Border border) {
		element.setBackground(background);
		element.setForeground(foreground);
		element.setBorder(border);
	}
	
	private void setThemeOlivia() {
		Color color, main, background, borderColor, secondary;
		setThemeDark();
		
		color = new Color(255, 204, 255);
		frame.getContentPane().setBackground(color);
		radioEnglish.setBackground(color);
		radioPolish.setBackground(color);
		checkboxDarkTheme.setBackground(color);
		
		main = new Color(255, 130, 255);
		background = new Color(18, 18, 18);
		secondary = new Color(225, 225, 225);
		borderColor = new Color(55, 54, 57);
		setButtonColor(main, background, secondary, borderColor);
		
		borderColor = main;
		
		labelIncorrectSyntax.setForeground(new Color(250, 75, 250));
		color = new Color(55, 54, 57);
		checkboxDarkTheme.setForeground(color);
		radioPolish.setForeground(color);
		radioEnglish.setForeground(color);
		lastFormula.setForeground(color);

		setElementColors(buttonResult, main, background, borderColor);
		setElementColors(buttonCE, main, background, borderColor);
		setElementColors(decimalFractionResultToggle, main, background, borderColor);
		
		theme = "olivia";
		labelTheme.setText(theme);
	}
	
	private void setThemeJula() {
		Color color;
		setThemeDefault();
		
		color = new Color(204, 255, 230);
		frame.getContentPane().setBackground(color);
		radioEnglish.setBackground(color);
		radioPolish.setBackground(color);
		checkboxDarkTheme.setBackground(color);
		
		theme = "jula";
		labelTheme.setText(theme);
	}
	
	private void formulaChanged() {
		String currentFormula = textField.getText();
		
		if(FractionCalculator.validFormula(currentFormula)) {
			errorPresent = false;
			labelIncorrectSyntax.setText("");
		}
		else {
			errorPresent = true;
			labelIncorrectSyntax.setText(localizedError);
		}
		
		if(resultPresent) resultPresent = false;
	}
	
	private void deleteLastCharFromTextField() {
		String currentFormula = textField.getText();
		if((currentFormula != null) && (currentFormula.length() > 0)) {
			currentFormula = currentFormula.substring(0, currentFormula.length() - 1);
		}
		textField.setText(currentFormula);
	}
	
	private void resetCalculator() {
		lastFormula.setText("");
		textField.setText("");
	}
	
	private void addCharacterToTextField(char c) {
		textField.setText(textField.getText() + c);
	}
	
	private void setLanguage(int lang) {
		switch(lang) {
		case 1:
			localizedError = polishError;
			localizedDecimalToggle = polishDecimalToggle;
			localizedDarkThemeCheckbox = polishDarkThemeCheckbox;
			break;
		case 0:
		default:
			localizedError = englishError;
			localizedDecimalToggle = englishDecimalToggle;
			localizedDarkThemeCheckbox = englishDarkThemeCheckbox;
			break;
		}
		
		if(errorPresent) {
			labelIncorrectSyntax.setText(localizedError);
		}
		
		decimalFractionResultToggle.setText(localizedDecimalToggle);
		checkboxDarkTheme.setText(localizedDarkThemeCheckbox);
	}
}
