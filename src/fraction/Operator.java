package fraction;

public class Operator implements Comparable< Operator > {
	char operator;
	int rank;
	boolean executed;
	Fraction toLeft;
	Fraction toRight;
	
	Operator(char operator, int rank) {
		this.operator = operator;
		this.rank = rank;
		this.executed = false;
	}
	
	Operator(char operator, int rank, Fraction toLeft) {
		this.operator = operator;
		this.rank = rank;
		this.executed = false;
		this.toLeft = toLeft;
	}
	
	Operator(char operator, int rank, Fraction toLeft, Fraction toRight) {
		this.operator = operator;
		this.rank = rank;
		this.executed = false;
		this.toLeft = toLeft;
		this.toRight = toRight;
	}
	
	public void setAsExecuted() {
		executed = true;
	}
	
	public boolean notExecuted() {
		return !executed;
	}
	
	public void setRightFraction(Fraction f) {
		toRight = f;
	}
	
	public void setLeftFraction(Fraction f) {
		toLeft = f;
	}
	
	public void decreaseRank(){
		rank--;
	}
	
	public void decreaseRank(int amount){
		rank -= amount;
	}
	
	public void increaseRank(){
		rank++;
	}
	
	public void increaseRank(int amount){
		rank += amount;
	}
	
	public Integer getRank() {
		return rank;
	}

	@Override
	public int compareTo(Operator o) {
		return this.getRank().compareTo(o.getRank());
	}
}
