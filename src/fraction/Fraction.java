package fraction;

import static java.lang.System.out;

public class Fraction {
	int wholePart;
	int numerator;
	int denominator;
	boolean negative;
	
	public Fraction() {}

	public Fraction(double d) {
		wholePart = 0;
		numerator = 0;
		denominator = 0;
		negative = false;

		parseDouble(d);
	}
	
	public Fraction(String s) {
		wholePart = 0;
		numerator = 0;
		denominator = 0;
		negative = false;
		
		if(!parseFractionString(s)) throw new IllegalArgumentException("Incorrect syntax");
		
		simplify();
	}
	
	public Fraction(int wholePart, int numerator, int denominator, boolean negative, boolean simplify) {
		this.wholePart = Math.abs(wholePart);
		this.numerator = Math.abs(numerator);
		this.denominator = Math.abs(denominator);
		if(!isZero()) this.negative = negative;
		
		if(simplify) simplify();
	}
	
	public Fraction(int wholePart, int numerator, int denominator, boolean negative) {
		this.wholePart = Math.abs(wholePart);
		this.numerator = Math.abs(numerator);
		this.denominator = Math.abs(denominator);
		if(!isZero()) this.negative = negative;
		simplify();
	}
	
	public Fraction(int wholePart, int numerator, int denominator) {		
		this.wholePart = wholePart;
		this.numerator = numerator;
		this.denominator = denominator;
		negative = false;
		
		if(newFractionIsNegative()) negative = true;
		
		this.wholePart = Math.abs(wholePart);
		this.numerator = Math.abs(numerator);
		this.denominator = Math.abs(denominator);
		
		simplify();
	}
	
	public boolean isNegative() {
		return negative;
	}
	
	public boolean isZero() {
		if(numerator == 0) return true;
		return false;
	}
	
	public void add(Fraction second) {
		if(isZero()) {
			wholePart = second.wholePart;
			numerator = second.numerator;
			denominator = second.denominator;
			negative = second.negative;
		}
		else {
			if(!second.isZero()) toCommonDenominator(second);
			
			minusToNumerator();
			second.minusToNumerator();
			
			numerator += second.numerator;
			
			minusToNegativeField();
			second.minusToNegativeField();
			
			simplify();
			second.simplify();
		}
	}
	
	public void substract(Fraction second) {
		if(isZero()) {
			wholePart = second.wholePart;
			numerator = second.numerator;
			denominator = second.denominator;
			negative = !second.negative;
		}
		else {
			if(!second.isZero()) toCommonDenominator(second);
			
			minusToNumerator();
			second.minusToNumerator();
			
			numerator -= second.numerator;
			
			minusToNegativeField();
			second.minusToNegativeField();
			
			simplify();
			second.simplify();
		}
	}
	
	public void power(Fraction f) {
		double x;
		x = f.toDouble();
		
		if(x % 2 == 0 && negative) negative = false;

		if(x < 0) {
			toReciprocal();
			x = -x;
		}
		else toImproper();

		numerator = (int) Math.pow(numerator, x);
		denominator = (int) Math.pow(denominator, x);
		simplify();
	}
	
	public void power(int x) {
		if(x % 2 == 0 && negative) negative = false;

		if(x < 0) {
			toReciprocal();
			x = -x;
		}
		else toImproper();

		numerator = (int) Math.pow(numerator, x);
		denominator = (int) Math.pow(denominator, x);
		simplify();
	}
	
	// Instance method version
	public void multiply(Fraction second) {
		// We work on improper fractions
		toImproper();
		second.toImproper();
		
		int negativeCounter = 0;
		if(isNegative()) negativeCounter++;
		if(second.isNegative()) negativeCounter++;
		if(negativeCounter % 2 == 0) negative = false;
		else negative = true;
		
		numerator *= second.numerator;
		denominator *= second.denominator;
		
		// And simplify them afterwards
		simplify();
		second.simplify();
		
		if(isZero()) wholePart = 1;
	}
	
	// Instance method version
	public void divide(Fraction second) {
		
		// (1/2) : (1/3) = (1/2) * (3)
		// Dividing a number is the same as multiplying its reciprocal
		second.toReciprocal();
		multiply(second);
		second.toReciprocal();
		second.simplify();
	}
	
	public void simplify() {
		if(!isZero()) {
			int greatestCommonDivisor = greatestCommonDivisor(numerator, denominator);
			numerator /= greatestCommonDivisor;
			denominator /= greatestCommonDivisor;
			
			// When the fraction simplifies to an integer
			if(denominator == 1 && numerator != 1) {
				wholePart += numerator;
				numerator = 1;
			}
			else if(numerator > denominator) {
				int newWholePart = numerator / denominator;
				wholePart += newWholePart;
				numerator %= denominator;
			}
		}
		else {
			negative = false;
		}
	}
	
	public void toCommonDenominator(Fraction second) {
		int numeratorMultiplication;
		int leastCommonMultiple;
		
		toImproper();
		second.toImproper();

		leastCommonMultiple = leastCommonMultiple(denominator, second.denominator);

		numeratorMultiplication = leastCommonMultiple / denominator;
		numerator *= numeratorMultiplication;
		denominator = leastCommonMultiple;
		
		numeratorMultiplication = leastCommonMultiple / second.denominator;
		second.numerator *= numeratorMultiplication;
		second.denominator = leastCommonMultiple;
	}
	
	
	public void toImproper() {
		simplify();
		
		if(wholePart != 0) {
			if(numerator == 1 && denominator == 1) numerator--;
			numerator += wholePart * denominator;
			wholePart = 0;
		}
	}
	
	public void toReciprocal() {
		toImproper();
		int aux = denominator;
		denominator = numerator;
		numerator = aux;
	}
	
	public void display() {
		String fractionString = toString();
		fractionString += " ";
		out.print(fractionString);
	}
	
	public String toString() {
		String strWholePart = Integer.toString(wholePart);
		String strNumerator = Integer.toString(numerator);
		String strDenominator = Integer.toString(denominator);
		String fractionString = "(";
		if(isNegative()) fractionString += "-";

		if(isZero()) fractionString += "0";
		else if(isOne()) fractionString += "1";
		else {
			// Display the whole part when it is different than 0
			if(wholePart != 0) fractionString += strWholePart;
			
			// Don't display the fraction when the numerator and the denominator both equal 1
			if(numerator != 1 || denominator != 1) {
				if(wholePart != 0) fractionString += " ";
				fractionString += strNumerator + "/" + strDenominator;
			}
		}
		
		fractionString += ")";
		
		return fractionString;
	}
	
	public double toDouble() {
		double convertedFraction;
		
		if(isZero()) return 0.0d;
		
		toImproper();
		minusToNumerator();
		convertedFraction = (double) numerator / denominator;
		minusToNegativeField();
		simplify();
		
		return convertedFraction;
	}
	
	public boolean isOne() {
		if(numerator == 1 && denominator == 1 && wholePart == 0) return true;
		return false;
	}
	
	public static boolean validSyntax(String s) {
		char c;
		int stage = 0;
		boolean openBracket = false, incorrectSyntax = false, end = false, 
				minusIsFree = true, digitPresent = false, isDouble = false,
				floatingPointIsPresent = false;
		
		for(int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			if(c == '.') {
				isDouble = true;
				break;
			}
		}
		
		if(isDouble) {
			for(int i = 0; i < s.length(); i++) {
				if(end) {
					incorrectSyntax = true;
					break;
				}
				c = s.charAt(i);
				
				if(openBracket) {
					if(c == '-') {
						if(!minusIsFree || digitPresent) {
							incorrectSyntax = true;
							break;
						}
						minusIsFree = false;
					}
					else if(Character.isDigit(c)) {
						digitPresent = true;
					}
					else if(c == '.') {
						if(floatingPointIsPresent || !digitPresent) {
							incorrectSyntax = true;
							break;
						}
						
						floatingPointIsPresent = true;
					}
					else if(c == ')') {
						openBracket = false;
						end = true;
					}
					else {
						incorrectSyntax = true;
						break;
					}
				}
				else if(c == '(') {
					openBracket = true;
				}
				else {
					incorrectSyntax = true;
					break;
				}
			}
		}
		else {
			for(int i = 0; i < s.length(); i++) {
				if(end) {
					incorrectSyntax = true;
					break;
				}
				c = s.charAt(i);
				
				if(openBracket) {
					if(stage == 0) {
						if(c == '-' && minusIsFree) {
							minusIsFree = false;
						}
						else if(Character.isDigit(c)) digitPresent = true; 
						else if(c == ' ' && digitPresent) {
							stage = 1;
							minusIsFree = true;
							digitPresent = false;
						}
						else if(c == '/' && digitPresent) {
							stage = 2;
							minusIsFree = true;
							digitPresent = false;
						}
						else if(c == ')' && digitPresent) {}
						else {
							incorrectSyntax = true;
							break;
						}
					}
					else if(stage == 1) {
						if(c == '-' && minusIsFree) {
							minusIsFree = false;
						}
						else if(Character.isDigit(c)) digitPresent = true;
						else if(c == '/' && digitPresent) {
							stage = 2;
							minusIsFree = true;
							digitPresent = false;
						}
						else {
							incorrectSyntax = true;
							break;
						}
					}
					else if(stage == 2) {
						if(c == '-' && minusIsFree) minusIsFree = false;
						else if(Character.isDigit(c)) digitPresent = true;
						else if(c == ')' && digitPresent) {}
						else {
							incorrectSyntax = true;
							break;
						}
					}
				}

				if(c == '(') {
					if(openBracket) { 
						incorrectSyntax = true;
						break;
					}
					openBracket = true;
				}
				else if(c == ')') {
					if(!openBracket) {
						incorrectSyntax = true;
						break;
					}
					openBracket = false;
					end = true;
				}
			}
		}

		if(openBracket) incorrectSyntax = true;
		
		return !incorrectSyntax;
	}
	
	private void minusToNumerator() {
		if(isNegative()) {
			negative = false;
			numerator = -numerator;
		}
	}
	
	private void minusToNegativeField() {
		if(numerator < 0) {
			negative = true;
			numerator = -numerator;
		}
	}
	
	private boolean newFractionIsNegative() {
		int minusCounter = 0;
		if(numerator < 0) minusCounter++;
		if(denominator < 0) minusCounter++;
		if(wholePart < 0) minusCounter++;
		
		if(minusCounter % 2 == 0) return false;
		return true;
	}
	
	
	// Greatest Common Divisor algorithm
	private static int greatestCommonDivisor(int first, int second) {
		if(second == 0)
			return first;
		else
			return greatestCommonDivisor(second, first % second);
	}
	
	private static int leastCommonMultiple(int first, int second) {
		int result = (first * second) / greatestCommonDivisor(first, second);
		
		return result;
	}
	
	private double floatingPointPart(double number) {
		String result = "0.";
		String numberString = Double.toString(number);
		boolean pastDecimalSeparator = false;
		char c;
		
		for(int i = 0; i < numberString.length(); i++) {
			c = numberString.charAt(i);
			if(pastDecimalSeparator) result += c;
			else if(c == '.') pastDecimalSeparator = true;
		}
		
		return Double.parseDouble(result);
	}
	
	private void parseDouble(double d) {
		if(d != 0d){
			if(d < 0){
				d = Math.abs(d);
				negative = true;
			}
			wholePart = (int) d;
			int newDenominator = 1;
			double floatingPointPart = floatingPointPart(d);
	
			while(floatingPointPart > 0){
				floatingPointPart *= newDenominator;
				floatingPointPart = floatingPointPart(floatingPointPart);
				newDenominator *= 10;
			}
	
			numerator = (int) ((floatingPointPart(d)) * newDenominator);
			denominator = newDenominator;
			
			if(numerator == 0) numerator = 1;
	
			simplify();
		}
	}
	
	private boolean parseFractionString(String s) {
		char c;
		int stage = 0;
		String numberString = "";
		boolean openBracket = false, incorrectSyntax = false, end = false, 
				minusIsFree = true, digitPresent = false, isDouble = false,
				floatingPointIsPresent = false;
		
		
		for(int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			if(c == '.') {
				isDouble = true;
				break;
			}
		}
		
		if(isDouble) {
			for(int i = 0; i < s.length(); i++) {
				if(end) {
					incorrectSyntax = true;
					break;
				}
				c = s.charAt(i);
				
				if(openBracket) {
					if(c == '-') {
						if(!minusIsFree || digitPresent) {
							incorrectSyntax = true;
							break;
						}
						minusIsFree = false;
						numberString += c;
					}
					else if(Character.isDigit(c)) {
						digitPresent = true;
						numberString += c;
					}
					else if(c == '.') {
						if(floatingPointIsPresent || !digitPresent) {
							incorrectSyntax = true;
							break;
						}
						
						floatingPointIsPresent = true;
						numberString += c;
					}
					else if(c == ')') {
						openBracket = false;
						end = true;
					}
					else {
						incorrectSyntax = true;
						break;
					}
				}
				else if(c == '(') {
					openBracket = true;
				}
				else {
					incorrectSyntax = true;
					break;
				}
			}
			
			if(!openBracket && !incorrectSyntax) {
				double doubleFraction = Double.parseDouble(numberString);
				parseDouble(doubleFraction);
			}
		}
		else {
			for(int i = 0; i < s.length(); i++) {
				if(end) {
					incorrectSyntax = true;
					break;
				}
				c = s.charAt(i);
				
				if(openBracket) {
					if(stage == 0) {
						if(c == '-' && minusIsFree) {
							negative = true;
							minusIsFree = false;
						}
						else if(Character.isDigit(c)) { numberString += c; digitPresent = true; } 
						else if(c == ' ' && digitPresent) {
							stage = 1;
							wholePart =  Integer.parseInt(numberString);
							numberString = "";
							minusIsFree = true;
							digitPresent = false;
						}
						else if(c == '/' && digitPresent) {
							stage = 2;
							numerator = Integer.parseInt(numberString);
							numberString = "";
							minusIsFree = true;
							digitPresent = false;
						}
						else if(c == ')' && digitPresent) {
							numerator = Integer.parseInt(numberString);
							denominator = 1;
						}
						else {
							incorrectSyntax = true;
							break;
						}
					}
					else if(stage == 1) {
						if(c == '-' && minusIsFree) {
							negative = !negative;
							minusIsFree = false;
						}
						else if(Character.isDigit(c)) { numberString += c; digitPresent = true; } 
						else if(c == '/' && digitPresent) {
							stage = 2;
							numerator = Integer.parseInt(numberString);
							numberString = "";
							minusIsFree = true;
							digitPresent = false;
						}
						else {
							incorrectSyntax = true;
							break;
						}
					}
					else if(stage == 2) {
						if(c == '-' && minusIsFree) {
							negative = !negative;
							minusIsFree = false;
						}
						else if(Character.isDigit(c)) { numberString += c; digitPresent = true; }
						else if(c == ')' && digitPresent) {
							denominator = Integer.parseInt(numberString);
						}
						else {
							incorrectSyntax = true;
							break;
						}
					}
				}

				if(c == '(') {
					if(openBracket) { 
						incorrectSyntax = true;
						break;
					}
					openBracket = true;
				}
				else if(c == ')') {
					if(!openBracket) {
						incorrectSyntax = true;
						break;
					}
					openBracket = false;
					end = true;
				}
			}
		}
		
		return !incorrectSyntax;
	}
}

