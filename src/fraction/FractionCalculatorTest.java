package fraction;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FractionCalculatorTest {

	@Test
	void testEvaluateExpression() {
		Fraction fraction = null;
		String expression;

		expression = "(-2) + (5) * (-5) + (1/2)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals("(-26 1/2)", fraction.toString());
		
		expression = "(9) / (3)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals("(3)", fraction.toString());
		
		expression = "(3 1/3) / (5/6)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals("(4)", fraction.toString());
		
		expression = "(-2.0) + (5) * (-5) + (0.5)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals("(-26 1/2)", fraction.toString());

		expression = "((-2.5) + (3.0))^(2.0) * (2.0) - (-0.5)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals("(1)", fraction.toString());
		
		expression = "((-2 1/2) + (3))^(2) * (2) - (-1/2)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals("(1)", fraction.toString());

		expression = "(-3) + (1/2) * ((2) * (4 1/2)^(2)) - (8) * (1/2)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals("(13 1/4)", fraction.toString());

		expression = "(((7/8) + (3/4)^(2))^(2))^(2)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals("(4 17697/65536)", fraction.toString());
		
		expression = "(((0.875) + (0.75)^(2.0))^(2))^(2)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals("(4 17697/65536)", fraction.toString());
		assertEquals(4.2700347900390625, fraction.toDouble());
		
		
		expression = "(-2) + (5) * (-5) + (1/2";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);
		
		expression = "(-2.0) + (5) ** (-5) + (0.5)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);

		expression = "((-2.5) + (3.0))^(2.0.0) * (2.0) - (-0.5)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);
		
		expression = "((-2 1/2 2) + (3))^(2) * (2) - (-1/2)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);
		
		expression = "(-2 1/2 + (3))^(2) * (2) - (-1/2)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);
		
		expression = "(2)(1 1/2)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);
		
		expression = "(2)(1 1/2) + (3)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);
		
		expression = "(2) * (1 1/2)(3)";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);
		
		expression = "(2) * (1 1/2)3434 3";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);
		
		expression = "(2) * (1 1/2)34";
		fraction = FractionCalculator.evaluateExpression(expression);
		assertEquals(null, fraction);
	}
	
	@Test
	void testValidFormula() {
		boolean result;
		String expression;

		expression = "(-2) + (5) * (-5) + (1/2)";
		result = FractionCalculator.validFormula(expression);
		assert result;
		
		expression = "(9) / (3)";
		result = FractionCalculator.validFormula(expression);
		assert result;
		
		expression = "(3 1/3) / (5/6)";
		result = FractionCalculator.validFormula(expression);
		assert result;
		
		expression = "(-2.0) + (5) * (-5) + (0.5)";
		result = FractionCalculator.validFormula(expression);
		assert result;

		expression = "((-2.5) + (3.0))^(2.0) * (2.0) - (-0.5)";
		result = FractionCalculator.validFormula(expression);
		assert result;
		
		expression = "((-2 1/2) + (3))^(2) * (2) - (-1/2)";
		result = FractionCalculator.validFormula(expression);
		assert result;

		expression = "(-3) + (1/2) * ((2) * (4 1/2)^(2)) - (8) * (1/2)";
		result = FractionCalculator.validFormula(expression);
		assert result;

		expression = "(((7/8) + (3/4)^(2))^(2))^(2)";
		result = FractionCalculator.validFormula(expression);
		assert result;
		
		expression = "(((0.875) + (0.75)^(2.0))^(2))^(2)";
		result = FractionCalculator.validFormula(expression);
		assert result;
		
		expression = "(-2) + (5) * (-5) + (1/2";
		result = FractionCalculator.validFormula(expression);
		assert !result;
		
		expression = "(-2.0) + (5) ** (-5) + (0.5)";
		result = FractionCalculator.validFormula(expression);
		assert !result;

		expression = "((-2.5) + (3.0))^(2.0.0) * (2.0) - (-0.5)";
		result = FractionCalculator.validFormula(expression);
		assert !result;
		
		expression = "((-2 1/2 2) + (3))^(2) * (2) - (-1/2)";
		result = FractionCalculator.validFormula(expression);
		assert !result;
		
		expression = "(-2 1/2 + (3))^(2) * (2) - (-1/2)";
		result = FractionCalculator.validFormula(expression);
		assert !result;
		
		expression = "-2 1/2 + (3)^(2) * (2) - (-1/2)";
		result = FractionCalculator.validFormula(expression);
		assert !result;
		
		expression = "(2)(1 1/2)";
		result = FractionCalculator.validFormula(expression);
		assert !result;
		
		expression = "(2)(1 1/2) + (3)";
		result = FractionCalculator.validFormula(expression);
		assert !result;
		
		expression = "(2) * (1 1/2)(3)";
		result = FractionCalculator.validFormula(expression);
		assert !result;
		
		expression = "(2) * (1 1/2)3434 3";
		result = FractionCalculator.validFormula(expression);
		assert !result;
		
		expression = "(2) * (1 1/2)34";
		result = FractionCalculator.validFormula(expression);
		assert !result;
	}

}
