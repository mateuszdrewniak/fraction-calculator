package fraction;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class OperatorTest {

	@Test
	void test() {
		Operator operator;
		Fraction fraction = new Fraction("(-2)");

		operator = new Operator('+', 2);
		operator.increaseRank(2);
		assertEquals(4, operator.rank);
		assertEquals(4, operator.getRank());
		operator.decreaseRank(2);
		assertEquals(2, operator.rank);
		operator.decreaseRank();
		assertEquals(1, operator.rank);
		operator.increaseRank();
		assertEquals(2, operator.rank);
		assert operator.notExecuted();
		operator.setAsExecuted();
		assert !operator.notExecuted();
		assertEquals(null, operator.toLeft);
		assertEquals(null, operator.toRight);
		operator.setLeftFraction(fraction);
		assertEquals(fraction, operator.toLeft);
		assertEquals(null, operator.toRight);
		operator.setRightFraction(fraction);
		assertEquals(fraction, operator.toLeft);
		assertEquals(fraction, operator.toRight);
	}

}
