package fraction;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class FractionCalculator {

	public static void main(String[] args) {
		out.println("Welcome to the Fraction Calculator");
		out.println("===================================");
		out.println("Submit a valid formula (+; -; /; *)");
		out.println("Every fraction/number has to be surrounded by parentheses");
		out.println("Example:\n(-3) + (1/2) * ((2) * (4 1/2)^(2)) - (8) * (1/2)");
		runCalculatorLoop();
		out.println("Fraction Calculator is shutting down...");
	}
	
	private static void runCalculatorLoop() {
		String continueString;
		Scanner scanner = new Scanner(System.in);

		do {
			out.println("\nEnter the formula:");
			parseLine(scanner.nextLine());
			out.println("Do you want to submit another formula? (Y/N)");
			continueString = scanner.nextLine().toLowerCase();
		} while(continueString.equals("y"));
		
		scanner.close();
	}
	
	private static void parseLine(String line) {
		char c;
		boolean incorrectSyntax = false;
		int leftBracketCounter = 0, rightBracketCounter = 0;

		for(int i = 0; i < line.length(); i++) {
			c = line.charAt(i);
			
			if(c == '(') leftBracketCounter++;
			else if(c == ')') rightBracketCounter++;
			else if(incorrectCharacter(c)) {
				incorrectSyntax = true;
				break;
			}
		}
		
		if(leftBracketCounter != rightBracketCounter || incorrectSyntax) incorrectSyntax = true;
		else {
			if(!analyzeLine(line)) incorrectSyntax = true;
		}
		
		if(incorrectSyntax) out.println("Incorrect syntax!\n------------------");
	}
	
	private static boolean incorrectCharacter(char c) {
		if(Character.isDigit(c)) {
			return false;
		}
		else {
			switch(c) {
			case '+':
			case '-':
			case '/':
			case '*':
			case '^':
			case '.':
			case ' ':
				return false;
			default:
				return true;
			}
		}
		
	}
	
	private static boolean analyzeLine(String s) {
		Fraction fraction = evaluateExpression(s);
		if(fraction == null) return false;
		
		out.println(fraction.toString());
		
		return true;
	}
	
	public static boolean validFormula(String s) {	
		int currentRank = 0;
		String fractionString = "";
		char c;
		boolean incorrectSyntax = false, insideOfAFraction = false,
				minusRegistered = false, operatorAwaitingAnArgument = false,
				lastCharIsAClosedBracket = false;
		
		for(int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			
			if(insideOfAFraction) {
				if(c == ')') {
					fractionString = "(" + fractionString + ")";
					
					if(!Fraction.validSyntax(fractionString)) {
						incorrectSyntax = true;
						break;
					}

					lastCharIsAClosedBracket = true;
					fractionString = "";
					insideOfAFraction = false;
					operatorAwaitingAnArgument = false;
					minusRegistered = false;
					currentRank -= 3;
				}
				else {
					fractionString += c;
				}
			}
			else if(c == '(') {
				if(lastCharIsAClosedBracket) {
					incorrectSyntax = true;
					break;
				}
				currentRank += 3;
				if(minusRegistered) minusRegistered = false;
			}
			else if (c == ')') {
				lastCharIsAClosedBracket = true;
				if(currentRank == 0) {
					incorrectSyntax = true;
					break;
				}
				currentRank -= 3;
			}
			else if(Character.isDigit(c) || c == '.') {
				if(currentRank == 0) {
					incorrectSyntax = true;
					break;
				}
				
				if(!insideOfAFraction) {
					insideOfAFraction = true;
					operatorAwaitingAnArgument = false;
					if(minusRegistered) {
						fractionString += "-";
						minusRegistered = false;
					}
				}
				lastCharIsAClosedBracket = false;
				fractionString += c;
			}
			else if(c == '-') {
				lastCharIsAClosedBracket = false;
				minusRegistered = true;
				operatorAwaitingAnArgument = true;
			}
			else if(charIsAnOperator(c)) {
				if(operatorAwaitingAnArgument || insideOfAFraction) {
					incorrectSyntax = true;
					break;
				}
				
				lastCharIsAClosedBracket = false;
				operatorAwaitingAnArgument = true;
			}
			else if(c != ' ') {
				incorrectSyntax = true;
				break;
			}
			
		}
		
		if(operatorAwaitingAnArgument || insideOfAFraction || incorrectSyntax) return false;
		
		return true;
	}
	
	static Fraction evaluateExpression(String s) {
		ArrayList<Fraction> fractions = new ArrayList<Fraction>();
		ArrayList<Operator> operators = new ArrayList<Operator>();
		
		Fraction fraction = null, fraction1, fraction2;
		Operator operator, newOperator;
		String fractionString = "";
		
		int currentRank = 0;
		char c;
		boolean incorrectSyntax = false, insideOfAFraction = false,
				minusRegistered = false, operatorAwaitingAnArgument = false,
				atLeastOneFraction = false, lastCharIsAClosedBracket = false;
		
		for(int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			
			if(insideOfAFraction) {
				if(c == ')') {
					fractionString = "(" + fractionString + ")";
					
					if(!Fraction.validSyntax(fractionString)) {
						incorrectSyntax = true;
						break;
					}
					
					fraction = new Fraction(fractionString);
					fractions.add(fraction);
					if(atLeastOneFraction)
						operators.get(operators.size() - 1).toRight = fraction;

					lastCharIsAClosedBracket = true;
					fractionString = "";
					insideOfAFraction = false;
					operatorAwaitingAnArgument = false;
					minusRegistered = false;
					currentRank -= 3;
					atLeastOneFraction = true;
				}
				else {
					fractionString += c;
				}
			}
			else if(c == '(') {
				if(lastCharIsAClosedBracket) {
					incorrectSyntax = true;
					break;
				}

				currentRank += 3;
				if(minusRegistered) minusRegistered = false;
			}
			else if (c == ')') {
				lastCharIsAClosedBracket = true;
				if(currentRank == 0) {
					incorrectSyntax = true;
					break;
				}
				currentRank -= 3;
			}
			else if(Character.isDigit(c) || c == '.') {
				if(currentRank == 0) {
					incorrectSyntax = true;
					break;
				}
				
				if(!insideOfAFraction) {
					insideOfAFraction = true;
					operatorAwaitingAnArgument = false;
					if(minusRegistered) {
						fractionString += "-";
						minusRegistered = false;
						operators.remove(operators.size() - 1);
					}
				}
				lastCharIsAClosedBracket = false;
				fractionString += c;
			}
			else if(c == '-') {
				minusRegistered = true;
				operatorAwaitingAnArgument = true;
				operator = new Operator('-', currentRank, fraction);
				operators.add(operator);
				lastCharIsAClosedBracket = false;
			}
			else if(charIsAnOperator(c)) {
				if(operatorAwaitingAnArgument || insideOfAFraction) {
					incorrectSyntax = true;
					break;
				}
				
				lastCharIsAClosedBracket = false;
				operatorAwaitingAnArgument = true;
				operator = new Operator(c, calculateRank(currentRank, c), fraction);
				operators.add(operator);
			}
			else if(c != ' ') {
				incorrectSyntax = true;
				break;
			}
			
		}
		
		if(operatorAwaitingAnArgument || insideOfAFraction || incorrectSyntax) return null;
		
		Collections.sort(operators, Collections.reverseOrder());
		
		for(int i = 0; i < operators.size(); i++) {
			operator = operators.get(i);
			fraction1 = operator.toLeft;
			fraction2 = operator.toRight;

			switch(operator.operator) {
				case '+':
					fraction1.add(fraction2);
					break;
				case '-':
					fraction1.substract(fraction2);
					break;
				case '*':
					fraction1.multiply(fraction2);
					break;
				case '/':
					fraction1.divide(fraction2);
					break;
				case '^':
					fraction1.power(fraction2);
					break;
			}
			fraction = fraction1;
			operator.setAsExecuted();
			if(i < operators.size() - 1) {
				newOperator = findOperatorWithLeft(operators, fraction2);
				if(newOperator != null)
					newOperator.toLeft = operator.toLeft;
			}
			fractions.remove(fraction2);
		}
		if(incorrectSyntax) return null;
		
		return fraction;
	}
	
	private static Operator findOperatorWithLeft(ArrayList<Operator> operators, Fraction f) {		
		Operator operator = null;
		for(int i = 0; i < operators.size(); i++) {
			operator = operators.get(i);
			if(operator.toLeft == f && operator.notExecuted()) return operator; 
		}
		
		return null;
	}
	
	private static boolean charIsAnOperator(char c) {
		switch(c) {
			case '+':
			case '-':
			case '*':
			case '/':
			case '^':
				return true;
			default:
				return false;
		}
	}
	
	private static int calculateRank(int currentRank, char operator) {
		switch(operator) {
			case '+':
			case '-':
				return currentRank;
			case '*':
			case '/':
				return currentRank + 1;
			case '^':
				return currentRank + 2;
			default:
				return currentRank;
		}
	}
}
