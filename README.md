# Fraction Calculator

One of my **first** Java applications. Coded using **Eclipse**. 2 532+ lines of code.
To run  the app open the .jar file that has been generated for you in the ./jar folder of this repo.

It's a calculator capable of (**+**; **-**; **\***; **/**; **^**) **integers, floating point numbers** and **common fractions**.

Has live syntax checking which runs whenever you change the formula.

Every number/fraction has to be surrounded by parentheses.

Examples:

* (-3) + (0.5) * ((2) * (4 1/2)^(2)) - (8) * (1/2)
* ((-2.5) + (3.0))^(2.0) * (2.0) - (-0.5)


To run on Windows and MacOS:
---------------
* Download and install java from `https://www.java.com/en/download/` (if you don't have it already)
* Download the `FractionCalculator.jar` file from `https://gitlab.com/Verseth/fraction-calculator/raw/master/jar/FractionCalculator.jar`
* Or if you've already got this repo cloned, you can find this file by navigating to the repo and entering the `jar` folder
* Simply double-click on `FractionCalculator.jar` to run this programme (you can move this file wherever you want)

To run on Linux:
---------------
If you haven't got a JRE configured yet, do the following:
* `$ sudo apt install default-jre` to install a JRE
* `$ sudo apt-get install jarwrapper` to be able to run `.jar` files by calling them by name
* Download the `FractionCalculator.jar` file from the repo with `$ wget https://gitlab.com/Verseth/fraction-calculator/raw/master/jar/FractionCalculator.jar`
* Or if you've already got this repo cloned, you can find this file by navigating to the repo and entering the `jar` folder
* Now you can run this programme by navigating to its location on your machine and entering `$ ./FractionCalculator.jar`

Some images
===========
Windows:
-----
<div>
    <img width="270" src="./img/fracCalcLight1.PNG" alt="Fraction Calculator PC Light">
    <img width="270" src="./img/fracCalcLight2.PNG" alt="Fraction Calculator PC Light Decimal Mode">
    <img width="270" src="./img/fracCalcLight3.PNG" alt="Fraction Calculator PC Light Wrong Syntax">
</div>

<div>
    <img width="270" src="./img/fracCalcDark1.PNG" alt="Fraction Calculator PC Dark">
    <img width="270" src="./img/fracCalcDark2.PNG" alt="Fraction Calculator PC Dark Decimal Mode">
    <img width="270" src="./img/fracCalcDark3.PNG" alt="Fraction Calculator PC Dark Wrong Syntax">
</div>

Linux:
-----
<div>
    <img width="270" src="./img/fracCalcUbuntuLight1.png" alt="Fraction Calculator Ubuntu Light">
    <img width="270" src="./img/fracCalcUbuntuLight2.png" alt="Fraction Calculator Ubuntu Light Decimal Mode">
    <img width="270" src="./img/fracCalcUbuntuLight3.png" alt="Fraction Calculator Ubuntu Light Wrong Syntax">
</div>

<div>
    <img width="270" src="./img/fracCalcUbuntuDark1.png" alt="Fraction Calculator Ubuntu Dark">
    <img width="270" src="./img/fracCalcUbuntuDark2.png" alt="Fraction Calculator Ubuntu Dark Decimal Mode">
    <img width="270" src="./img/fracCalcUbuntuDark3.png" alt="Fraction Calculator Ubuntu Dark Wrong Syntax">
</div>

Mac:
-----
<div>
    <img width="270" src="./img/fracCalcMacLight1.png" alt="Fraction Calculator Mac Light">
    <img width="270" src="./img/fracCalcMacLight2.png" alt="Fraction Calculator Mac Light Decimal Mode">
    <img width="270" src="./img/fracCalcMacLight3.png" alt="Fraction Calculator Mac Light Wrong Syntax">
</div>
